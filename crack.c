#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be 
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *h = md5(guess, strlen(guess));

    // Compare the two hashes
    if(strcmp(h, hash) == 0) {
        return 1;
    }

    // Free any malloc'd memory
    free(h);
    return 0;
}

// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
    struct stat info;
    stat(filename, &info);
    int flength = info.st_size;

    char *data = malloc(flength + 1);
    data[flength] = '\0';

    FILE *hashfile = fopen(filename, "r");
    if(!hashfile) {
        perror("Cannot open hash file");
        exit(1);
    }

    fread(data, 1, flength, hashfile);
    fclose(hashfile);
    
    int lines = 0;
    for(int i = 0; i < flength; i++) {
        if(data[i] == '\n') {
            data[i] = '\0';
            lines++;
        }
    }

    char **strings = malloc(((lines + 1) * sizeof(char *)));
    int string_idx = 0;
    for(int i = 0; i < flength; i += strlen(data + i) + 1) {
        strings[string_idx] = data + i;
        string_idx++;
    }

    strings[string_idx] = NULL;
    
    return strings;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.

// >>>I THINK THIS IS DONE<<<
char **read_dict(char *filename)
{
    struct stat info;
    stat(filename, &info);
    int flength = info.st_size;

    char *data = malloc(flength + 1);
    FILE *f = fopen(filename, "r");
    if(!f) {
        perror("Can't open file");
        exit(1);
    }

    fread(data, 1, flength, f);
    fclose(f);

    data[flength] = '\0';

    int lines = 0;
    for(int i = 0; i < flength; i++) {
        if(data[i] == '\n') {
            data[i] = '\0';
            lines++;
        }
    }
    
    // other arrays and such that were added during testing, could be optimized more...
    char **stringscat = malloc(((lines + 1 + HASH_LEN) * sizeof(char *)));
    char **strings = malloc( ((lines+1) * sizeof(char *)) );
    int string_idx = 0;
    
    for(int i = 0; i < flength; i += strlen(data + i) + 1) {
        strings[string_idx] = data + i;
        char *concat = malloc(100);

        strcpy(concat, strings[string_idx]);
        strcat(concat, ": ");
        stringscat[string_idx] = strcat(concat, md5(data + i, strlen(data + i)));
        //everything works until after this printf statement and exits the loop
        //printf("%s\n", stringscat[string_idx]);
        string_idx++;

    }

    // this printf turns out to just print the same thing over and over, loses all the previous data...
    /**
    for(int i = 0; i < 100; i++)
    {
        printf("stringscat[%d]: %s\n", i,  stringscat[i]);
    }
    */
    
    //add ending NULL character at the end
    stringscat[string_idx] = NULL;
    
    
    //returning this string since it is the concatenated pointer array string
    return stringscat;
}

int cmp_func(const void *a, const void *b)
{
    return strcmp(*((char **)a), *((char **)b));
}

int bcompare(const void *key, const void *elem)
{
    return strncmp((char *)key, *((char **)elem), strlen((char *)key));
}



int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    //qsort(dict, ___, ___, ___);
    
    //I think this doesnt work because of my previous code problems... PLEASE HELP!
    qsort(dict, 100, sizeof(char *), cmp_func);
    
    /**
    printf("Here's the array, sorted:\n");
    for (int i = 0; i < 100; i++)
    {
        printf("%s\n", dict[i]);
    }
    */
    
    


    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    // Search the array for a target
    
    // New problem, only finds one single hash/password, problem with bsearch??
    // Still not working...
    for (int a = 0; a < 100; a++)
    {
        char **found = bsearch(&hashes, dict, 100, sizeof(char *), bcompare);
        printf("Hash: %s\n", hashes[a]);
        if(found != NULL)
        {
            printf("Found the hash and password: %s\n", *found);
        }
        //printf("a: %d\n", a);
    }
    
}